class AddRowsToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :item, :string
    add_column :invoices, :description, :text
    add_column :invoices, :unit_cost, :decimal
    add_column :invoices, :quantity, :integer
    add_column :invoices, :price, :decimal
    add_column :invoices, :image, :string
  end
end

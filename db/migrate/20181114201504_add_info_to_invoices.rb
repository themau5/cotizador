class AddInfoToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :address_one, :text
    add_column :invoices, :address_two, :text
    add_column :invoices, :phonenumber, :integer
    add_column :invoices, :additinal, :text
    add_column :invoices, :logo, :string
  end
end
